# Lambda
# local deployment package
resource "aws_lambda_function" "lambda_function" {
  filename      =  "${var.deploy_mode == "s3" ? null : var.filename}"
  s3_bucket     = "${var.deploy_mode == "s3" ? var.s3_bucket : null}"
  s3_key        = "${var.deploy_mode == "s3" ? var.s3_key : null}"
  function_name = var.function_name
  role          = aws_iam_role.lambda.arn
  handler       = var.handler
  runtime       = "python3.7"
  timeout       = var.timeout
  memory_size   = var.memory_size
  source_code_hash =  "${var.deploy_mode == "s3" ? filebase64sha256(var.filename) : filebase64sha256(var.filename)}"
}


# IAM
resource "aws_iam_role" "lambda" {
  name = "qvntra_${var.function_name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

data "aws_iam_policy_document" "lambda" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
     "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid = "2"
    effect = "Allow"
    actions = [
      "dynamodb:*"
    ]
    resources = [
      "*",
    ]
  }

  statement {
    sid = "3"
    effect = "Allow"
    actions = [
      "s3:*"
    ]
    resources = [
      "*",
    ]
  }
}


resource "aws_iam_policy" "lambda" {
  name   = "qvntra_${var.function_name}"
  path   = "/"
  policy = data.aws_iam_policy_document.lambda.json
}

resource "aws_iam_role_policy_attachment" "lambda" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.lambda.arn
}


## cron lambda
resource "aws_cloudwatch_event_rule" "rule" {
    count = "${var.lambda_type == "cron" ? 1 : 0}"
    name = "${var.function_name}-cron-rule"
    description = "${var.function_name}-cron-rule"
    # rate (1 day)
    schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "target" {
    count = "${var.lambda_type == "cron" ? 1 : 0}"
    rule = aws_cloudwatch_event_rule.rule[0].name
    target_id = var.function_name
    arn = aws_lambda_function.lambda_function.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_function" {
    count = "${var.lambda_type == "cron" ? 1 : 0}"
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda_function.function_name
    principal = "events.amazonaws.com"
    source_arn = aws_cloudwatch_event_rule.rule[0].arn
}

