variable "filename" {
}
variable "deploy_mode" {
    description = "either local or s3"
    default = "local"
}
variable "function_name" {}
variable "handler" {}
variable "timeout" {
    type = number
    description = "min 3 seconds, max 15 minutes"
}
variable "memory_size" {
    type = number
    default = 128
}
variable "s3_bucket" {
}
variable "s3_key" {
}

# cron lambda
variable "lambda_type" {
    description = "if cron lambda or not"
    default = ""
}
variable "schedule_expression" {
    description = "if lambda_type is cron, schedule_expression should be provided"
    default = ""
}

